﻿using System;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Rigidbody))]
public class BotMovement : MonoBehaviour
{
    private readonly Vector3[] _directions = new Vector3[4];
    private readonly Vector3[] _rotations = new Vector3[4];
    
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private float _speedValue;
    [SerializeField] private float _minRandom;
    [SerializeField] private float _maxRandom;
    private Vector3 _currentRotation;
    private Material _startMaterial;

    private Shooting _player;

    [HideInInspector]
    public Vector3 _currentDirection;
    [HideInInspector]
    public float health;
    
    public event Action Died;
    private bool _canShowFinalUI;

    private void Awake()
    {
        health = GlobalSO.BotHealth;
        _player = FindObjectOfType<Shooting>();
        
        _startMaterial = GetComponent<MeshRenderer>().material;
        _directions[0] = Vector3.forward;
        _directions[1] = Vector3.back;
        _directions[2] = Vector3.right;
        _directions[3] = Vector3.left;

        _currentDirection = _directions[Random.Range(0, 4)];
        
        _rotations[0] = new Vector3(0, 0, 0);
        _rotations[1] = new Vector3(0, 180, 0);
        _rotations[2] = new Vector3(0, 90, 0);
        _rotations[3] = new Vector3(0, 270, 0);
    }

    private void OnEnable()
    {
        GetComponent<MeshRenderer>().material = _startMaterial;
    }

    private float _timer;
    private float _randomTime;

    private void FixedUpdate()
    {
        _rigidbody.velocity = _currentDirection * _speedValue * Time.deltaTime;
    }

    private void Update()
    {
        _timer += Time.deltaTime;

        if (_timer >= _randomTime)
        {
            _timer = 0;
            PickRandomDirection();
        }

        if (health <= 0)
        {
            Died?.Invoke();
            InfoUI.amountEnemy--;
            FinalCounting.BOTS_KILLED++;

            if (InfoUI.amountEnemy <= 0)
            {

                if (SceneManager.GetActiveScene().buildIndex == 4)
                {
                    _player.StartWinGameCoroutine();
                    return;
                }
                Debug.Log("WIN LEVEL!");
                _player.StartWinLevelCoroutine();
            }
            
            
            gameObject.SetActive(false);
        }
    }

    private void PickRandomDirection()
    {
        const float rotationValue = 0.2f;

        int randomValue = Random.Range(0, 4);
        _currentDirection = _directions[randomValue];
        _currentRotation = _rotations[randomValue];


        transform.DORotate(_currentRotation, rotationValue);

        _randomTime = Random.Range(_minRandom, _maxRandom);

        if (Physics.Raycast(transform.position, _currentDirection, out RaycastHit hitInfo, 5))
        {
            if (hitInfo.distance <= 0.6f && (hitInfo.collider.CompareTag(GlobalTags.OBSTACLE_TAG) || hitInfo.collider.CompareTag(GlobalTags.WALL_TAG)))
            {
                PickRandomDirection();
            }
        }

    }
}
