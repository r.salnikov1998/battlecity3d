﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TESTSCRIPT : MonoBehaviour
{
    public Animator _fadeAnimation;
    public Animator _uiAnimation;
    public Animator _MainMenu;

    [ContextMenu("start Final Animation")]
    public void StartAnimation()
    {
        _uiAnimation.SetTrigger("FinalAnimation");
    }


    [ContextMenu("start nonFinal Animation")]
    public void StartNonFinalAnimation()
    {
        _fadeAnimation.SetTrigger("FadeAnimation");
    } 
    
    [ContextMenu("MainMenu")]
    public void MainMenu()
    {
        _MainMenu.SetTrigger("MainMenu");
    }
}
