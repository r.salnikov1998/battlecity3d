﻿using UnityEngine;

public abstract class ABullet : MonoBehaviour
{
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private float _speed;
    [SerializeField] private ParticleSystem _explosionWall;

    [HideInInspector]
    public Vector3 bulletDirection;

    protected virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag(GlobalTags.OBSTACLE_TAG))
        {
            var spawned = Instantiate(_explosionWall, transform.position, Quaternion.identity);
            Destroy(collision.gameObject);
            Destroy(gameObject, 0.005f);
        }

    }

    public void Move(Vector3 direction)
    {
        _rigidbody.AddForce(direction * _speed, ForceMode.Impulse);
        bulletDirection = direction;
    }
}