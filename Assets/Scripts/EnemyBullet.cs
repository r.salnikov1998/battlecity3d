﻿using System.Collections;
using UnityEngine;

public class EnemyBullet : ABullet
{
    [SerializeField] private ParticleSystem _citadelDieEffect;
    [SerializeField] private AudioSource[] _explosionSounds;

    protected override void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);

        if(collision.gameObject.TryGetComponent(out Shooting player))
        {
            player.health--;
            gameObject.SetActive(false);
        }

        if (collision.gameObject.CompareTag(GlobalTags.CITADEL_TAG))
        {
            Instantiate(_citadelDieEffect, collision.gameObject.transform.position, Quaternion.identity);
            _explosionSounds[Random.Range(0, _explosionSounds.Length)].Play();
            collision.gameObject.SetActive(false);
            FindObjectOfType<Shooting>().health--;
        }
        
        if (collision.collider.CompareTag(GlobalTags.WALL_TAG))
        {
            gameObject.SetActive(false);
        }
    }
}