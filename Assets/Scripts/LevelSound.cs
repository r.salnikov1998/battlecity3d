﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class LevelSound : MonoBehaviour
{
    [SerializeField] private AudioSource[] _sounds;
    [SerializeField] private AudioSource _enemyDown;
    public static AudioSource CurrentSound;

    private void Start()
    {
        RenderSettings.skybox.SetFloat("_Rotation", 0);

        CurrentSound = _sounds[SceneManager.GetActiveScene().buildIndex - 1];
        CurrentSound.Play();

        for (int i = 0; i < EnemySpawner._enemies.Count; i++)
        {
            EnemySpawner._enemies[i].Died += OnEnemyDied;
        }
    }

    private void OnEnemyDied()
    {
        _enemyDown.Play();
    }
}