﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private float _speedValue;
    [SerializeField] private float _rotationDuration;
    [SerializeField] private float _pushForce;

    private void FixedUpdate()
    {
        InputMovement();
        CorrectDirection();
    }

    private void InputMovement()
    {
        _rigidbody.velocity = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
        {
            _rigidbody.velocity = transform.forward * _speedValue * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            _rigidbody.velocity = -transform.forward * _speedValue * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            _rigidbody.velocity = transform.right * _speedValue * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            _rigidbody.velocity = -transform.right * _speedValue * Time.deltaTime;
        }
    }

    private void CorrectDirection()
    {
        Vector3 vector = transform.localEulerAngles;

        vector.x = Mathf.Round(vector.x / 90) * 90;
        vector.y = Mathf.Round(vector.y / 90) * 90;
        vector.z = Mathf.Round(vector.z / 90) * 90;

        transform.localEulerAngles = vector;
    }
}