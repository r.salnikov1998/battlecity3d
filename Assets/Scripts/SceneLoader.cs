﻿using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{
    public Animator transition;
    public float transitionTime;
    public Button button;
    public CanvasGroup pressText;

    private Tweener _tween;

    private void OnEnable()
    {
        button.onClick.AddListener(LoadNextLevel);
    }
    private void Update()
    {
        RenderSettings.skybox.SetFloat("_Rotation", Time.time * 1f);

    }
    private void Start()
    {
        _tween = pressText.DOFade(0, transitionTime).SetLoops(-1, LoopType.Yoyo);
    }

    private void LoadNextLevel()
    {
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
    }

    private IEnumerator LoadLevel(int levelIndex)
    {
        _tween.Kill();
        pressText.alpha = 1;
        transition.SetTrigger("Start");
        pressText.DOFade(0, transitionTime);

        yield return new WaitForSeconds(transitionTime);

        Debug.Log("should load next scene");
        SceneManager.LoadScene(levelIndex);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
