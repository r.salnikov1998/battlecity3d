﻿using UnityEngine;

public class BotShooting : MonoBehaviour
{
    [SerializeField] private ABullet _bulletPrefab;
    [SerializeField] private GameObject _bulletCreator;
    [SerializeField] private BotMovement _botMovement;
    [SerializeField] private float  _minTime;
    [SerializeField] private float  _maxTime;
    
    private float _timeToShoot;

    private float _timer;
    private void Update()
    {
        _timer += Time.deltaTime;
        
        if (_timer >= _timeToShoot)
        {
            _timeToShoot = Random.Range(_minTime, _maxTime);
            _timer = 0;
            var spawned = Instantiate(_bulletPrefab, _bulletCreator.transform.position, Quaternion.identity);
            spawned.Move(_botMovement._currentDirection);
        }
    }
}