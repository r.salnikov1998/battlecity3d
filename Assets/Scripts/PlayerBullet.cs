﻿using UnityEngine;

public class PlayerBullet : ABullet
{
    [SerializeField] private ParticleSystem _bloodEffect;
    [SerializeField] private ParticleSystem _hitEffect;
    [SerializeField] private AudioSource[] _ricochetSound;

    protected override void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);
        Shooting.canShoot = true;

        if (collision.gameObject.CompareTag(GlobalTags.WALL_TAG))
        {
            _ricochetSound[Random.Range(0, _ricochetSound.Length)].Play();
            Instantiate(_hitEffect, transform.position, Quaternion.identity);
            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<Collider>().enabled = false;
        }

        if (collision.gameObject.TryGetComponent(out BotMovement bot))
        {
            bot.health--;

            if (bot.health <= 0)
            {
                var bloodEffect = Instantiate(_bloodEffect, transform.position, Quaternion.identity);

                if (bulletDirection == Vector3.forward)
                {
                    bloodEffect.transform.localEulerAngles = new Vector3(0, 270, 0);
                }

                else if (bulletDirection == Vector3.back)
                {
                    bloodEffect.transform.localEulerAngles = new Vector3(0, 90, 0);
                }

                else if (bulletDirection == Vector3.right)
                {
                    bloodEffect.transform.localEulerAngles = new Vector3(0, 0, 0);
                }

                else if (bulletDirection == Vector3.left)
                {
                    bloodEffect.transform.localEulerAngles = new Vector3(0, 180, 0);
                }
            }
            else
            {
                // var color = bot.GetComponent<MeshRenderer>().material;
                // color.DOColor(Color.red, 0.3f).SetLoops(2, LoopType.Yoyo);
                Instantiate(_hitEffect, transform.position, Quaternion.identity);
            }

            gameObject.SetActive(false);
        }

        if (collision.gameObject.CompareTag(GlobalTags.CITADEL_TAG))
        {
            gameObject.SetActive(false);
        }
    }
}