﻿public static class GlobalTags
{
    public const string OBSTACLE_TAG = "Obstacle";
    public const string WALL_TAG = "Wall";
    public const string CITADEL_TAG = "Citadel";
    public const string SHOTS = "Shots";
    public const string BOTS = "Bots";
    
    public const string TOTAL_ATTEMPTS = "TotalAttempts";
    public const string BOTS_KILLED = "BotsKilled";
    public const string TOTAL_SHOTS = "TotalShots";
    public const string TOTAL_JUMPS = "TotalJumps";
    

}
