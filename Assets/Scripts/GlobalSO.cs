﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using CMF;

public class GlobalSO : MonoBehaviour
{
    public static float BotHealth;
    private TMP_Dropdown ShotsToKillDropDown;
    private TMP_Dropdown BotsAmount;
    public Toggle _toggle;

    private TMP_Dropdown[] _dropdowns;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        _dropdowns = GameObject.FindObjectsOfType(typeof(TMP_Dropdown)) as TMP_Dropdown[];

        foreach (TMP_Dropdown drop in _dropdowns)
        {
            if (drop.CompareTag(GlobalTags.SHOTS))
            {
                ShotsToKillDropDown = drop;
            }

            else if (drop.CompareTag(GlobalTags.BOTS))
            {
                BotsAmount = drop;
            }

        }
    }

    private void Start()
    {
        BotHealth = 1;
        InfoUI.amountEnemy = 10;
        InfoUI.SavedAmountEnemy = 10;
        ShotsToKillDropDown.onValueChanged.AddListener(delegate { DropdownValueChanged(ShotsToKillDropDown); });
        BotsAmount.onValueChanged.AddListener(delegate { BotChangedDropdown(BotsAmount); });
    }

    private void Update()
    {
        // Debug.Log("InfoUI.amountEnemy =" + InfoUI.amountEnemy);
        // Debug.Log("BotsAmount GlobalSO =" + BotsAmount.value);

        if (_toggle.isOn)
        {
            CharacterKeyboardInput.jumpKey = KeyCode.None;
        }
        else
        {
            CharacterKeyboardInput.jumpKey = KeyCode.Space;
        }
    }

    private void DropdownValueChanged(TMP_Dropdown change)
    {
        BotHealth = change.value + 1;
    }

    private void BotChangedDropdown(TMP_Dropdown change)
    {
        if (change.value == 0)
        {
            InfoUI.amountEnemy = 10;
            InfoUI.SavedAmountEnemy = 10;
        }

        else if (change.value == 1)
        {
            InfoUI.amountEnemy = 15;
            InfoUI.SavedAmountEnemy = 15;
        }

        else if (change.value == 2)
        {
            InfoUI.amountEnemy = 20;
            InfoUI.SavedAmountEnemy = 20;
        }
    }
}