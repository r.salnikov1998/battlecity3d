﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using Random = UnityEngine.Random;
using UnityEngine.SceneManagement;

public class Shooting : MonoBehaviour
{
    [SerializeField] private ABullet playerBulletPrefab;
    [SerializeField] private GameObject _bulletCreator;
    [SerializeField] private ParticleSystem _killPlayerParticle;
    [SerializeField] private AudioSource _shotSound;
    [SerializeField] private AudioSource _dieSound;
    [SerializeField] private AudioSource winLevelSound;
    [SerializeField] private AudioSource winGameSound;
    [SerializeField] private GameObject _meshRenderer;
    [SerializeField] private GameObject _gunMesh;
    [SerializeField] private Collider _collider;
    [SerializeField] private Image _gameOver;
    [SerializeField] private Animator _animator;
    [SerializeField] private Button _mainMenuButton;
    [SerializeField] private Button _tryAgain;

    [SerializeField] private CanvasGroup _pressToReturn;
    [SerializeField] private CanvasGroup _escape;
    [SerializeField] private CanvasGroup _keyboardControls;

    [SerializeField] private Animator BlackScreenAnimator;
    [SerializeField] private Animator FullInfoAnimator;
    [SerializeField] private Animator MainMenuAnimator;

    [SerializeField] private TMP_Text _text;

    private TMP_Text TotalAttempts;
    private TMP_Text BotsKilled;
    private TMP_Text TotalShots;
    private TMP_Text TotalJumps;

    public static bool canShoot;
    public float health;

    private bool _alive = true;

    private bool escapeBool;

    private IEnumerator WinGame()
    {
        var texts = FindObjectsOfType<TMP_Text>();

        foreach (var text in texts)
        {
            if (text.gameObject.CompareTag(GlobalTags.TOTAL_ATTEMPTS))
                TotalAttempts = text;

            if (text.gameObject.CompareTag(GlobalTags.BOTS_KILLED))
                BotsKilled = text;

            if (text.gameObject.CompareTag(GlobalTags.TOTAL_SHOTS))
                TotalShots = text;

            if (text.gameObject.CompareTag(GlobalTags.TOTAL_JUMPS))
                TotalJumps = text;
        }


        TotalAttempts.text = FinalCounting.TOTAL_ATTEMPTS.ToString();
        BotsKilled.text = FinalCounting.BOTS_KILLED.ToString();
        TotalShots.text = FinalCounting.TOTAL_SHOTS.ToString();
        TotalJumps.text = FinalCounting.TOTAL_JUMPS.ToString();


        var bots = FindObjectsOfType<BotShooting>();

        foreach (var bot in bots)
        {
            bot.gameObject.SetActive(false);
        }

        FindObjectOfType<EnemySpawner>().gameObject.SetActive(false);

        LevelSound.CurrentSound.Stop();
        LevelSound.CurrentSound = winGameSound;
        LevelSound.CurrentSound.Play();
        yield return new WaitForSeconds(1);
        BlackScreenAnimator.SetTrigger("FadeAnimation");
        yield return new WaitForSeconds(1);
        FullInfoAnimator.SetTrigger("FinalAnimation");
        yield return new WaitForSeconds(10);
        MainMenuAnimator.SetTrigger("MainMenu");
    }

    private void Start()
    {
        // Destroy(FindObjectOfType<GlobalSO>().gameObject);
        _mainMenuButton.gameObject.SetActive(false);
        _tryAgain.gameObject.SetActive(false);
        _keyboardControls.alpha = 1;
        Time.timeScale = 0;
        canShoot = true;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            FinalCounting.TOTAL_JUMPS++;
        }

        if (Input.GetMouseButtonDown(0) && canShoot && GlobalDirections.currentDirection != null)
        {
            FinalCounting.TOTAL_SHOTS++;
            var spawned = Instantiate(playerBulletPrefab, _bulletCreator.transform.position, Quaternion.identity);
            spawned.Move(GlobalDirections.currentDirection);
            _shotSound.pitch = Random.Range(0.8f, 1.2f);
            _shotSound.Play();
            canShoot = false;
        }

        if (Input.GetMouseButtonDown(1))
        {
            _keyboardControls.alpha = 0;
            Time.timeScale = 1;
        }

        if (health <= 0 && _alive)
        {
            _alive = false;
            Instantiate(_killPlayerParticle, transform.position, Quaternion.identity);
            _meshRenderer.SetActive(false);
            _gunMesh.SetActive(false);
            _collider.enabled = false;
            StartCoroutine(Die());
        }

        if (Input.GetKeyDown(KeyCode.Escape) && !escapeBool)
        {
            _mainMenuButton.gameObject.SetActive(true);
            escapeBool = true;
            _escape.alpha = 0.5f;
            Time.timeScale = 0;
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && escapeBool)
        {
            _mainMenuButton.gameObject.SetActive(false);
            escapeBool = false;
            _escape.alpha = 0;
            Time.timeScale = 1;
        }
    }

    public IEnumerator Die()
    {
        yield return new WaitForSeconds(1.5f);
        LevelSound.CurrentSound.Stop();
        _dieSound.Play();
        _animator.SetTrigger("Start");
        yield return new WaitForSeconds(1);
        _gameOver.transform.DOLocalMoveY(50, 2).SetEase(Ease.InOutBack);
        yield return new WaitForSeconds(2);
        _pressToReturn.DOFade(1, 1);
        _tryAgain.gameObject.SetActive(true);
    }

    public void ReturnMenu()
    {
        _escape.alpha = 0;
        Time.timeScale = 1;
        StartCoroutine(Wait1sec());
    }

    public void StartWinGameCoroutine()
    {
        StartCoroutine(WinGame());
    }

    public void TryAgain()
    {
        FinalCounting.TOTAL_ATTEMPTS++;
        _escape.alpha = 0;
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void StartWinLevelCoroutine()
    {
        StartCoroutine(WinLevel());
    }

    private IEnumerator WinLevel()
    {
        _animator.SetTrigger("Start");
        LevelSound.CurrentSound.Stop();
        LevelSound.CurrentSound = winLevelSound;
        LevelSound.CurrentSound.Play();
        var bots = FindObjectsOfType<BotShooting>();

        foreach (var bot in bots)
        {
            bot.gameObject.SetActive(false);
        }

        yield return new WaitForSeconds(1);
        _text.gameObject.SetActive(true);
        _text.text = "Level " + (SceneManager.GetActiveScene().buildIndex + 1);

        _text.gameObject.transform.DOScale(Vector3.one * 3, 2).SetEase(Ease.InOutBack);


        yield return new WaitForSeconds(4);
        _text.gameObject.SetActive(false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private IEnumerator Wait1sec()
    {
        _animator.SetTrigger("Start");
        _mainMenuButton.gameObject.SetActive(false);
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(0);
    }
}