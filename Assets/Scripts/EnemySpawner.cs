﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private Transform[] _spawnPoints;
    [SerializeField] private BotMovement[] _enemyPrefab;
    [SerializeField] private float _timeToSpawn;
    [SerializeField] private GameObject _spawnEffect;

    public static List<BotMovement> _enemies;

    private float _timer;

    private void Awake()
    {
        _enemies = new List<BotMovement>();

        for (int i = 0; i < _spawnPoints.Length; i++)
        {
            var spawned = Instantiate(_enemyPrefab[Random.Range(0, _enemyPrefab.Length)], _spawnPoints[i].position,
                Quaternion.identity);
            _enemies.Add(spawned);
            StartCoroutine(SpawnBot(_spawnPoints[i].position, spawned));
        }
    }

    private void Update()
    {
        _timer += Time.deltaTime;

        if (_timer >= _timeToSpawn)
        {
            _timer = 0;
            ActiveEnemy();
        }
    }

    private void ActiveEnemy()
    {
        var result = _enemies.FirstOrDefault(p => p.gameObject.activeSelf == false);

        if (result == null)
        {
            return;
        }

        var spawnPointPosition = _spawnPoints[Random.Range(0, _spawnPoints.Length)].position;
        result.GetComponent<BotMovement>().health = GlobalSO.BotHealth;
        StartCoroutine(SpawnBot(spawnPointPosition, result));
    }


    private IEnumerator SpawnBot(Vector3 position, BotMovement bot)
    {
        bot.gameObject.SetActive(false);
        var spawned = Instantiate(_spawnEffect, position, _spawnEffect.transform.rotation);
        yield return new WaitForSeconds(1);
        bot.gameObject.SetActive(true);
        bot.transform.position = position;
        yield return new WaitForSeconds(0.5f);
        spawned.gameObject.SetActive(false);
    }
}