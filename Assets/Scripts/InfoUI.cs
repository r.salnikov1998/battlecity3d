﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InfoUI : MonoBehaviour
{
    public TMP_Text levelText;
    public static int amountEnemy;
    public TMP_Text enemyText;

    public static int SavedAmountEnemy;

    private void Start()
    {
        amountEnemy = SavedAmountEnemy;
        levelText.text = "Level " + SceneManager.GetActiveScene().buildIndex;
    }

    private void Update()
    {
        enemyText.text = amountEnemy.ToString();
    }
    
}
