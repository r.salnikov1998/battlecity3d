﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class FinalCounting : MonoBehaviour
{
    public static float TOTAL_ATTEMPTS = 1;
    public static float BOTS_KILLED = 0;
    public static float TOTAL_SHOTS = 0;
    public static float TOTAL_JUMPS = 0;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
}
